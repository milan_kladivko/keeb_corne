

oled_rotation_t oled_init_user(oled_rotation_t leave_it_default) { // QMK callback
    if (!is_keyboard_master()) {
        return leave_it_default;
    }
    return OLED_ROTATION_270;
}

// NOTE( SCREEN WRAPPING & CLEARING ):
//   - The screen is NOT wiped with every loop. 
//   - You must append empty lines yourself to clear the screen. 

// Size of screen when WIDE, by default mono font
//#define OLED_CHARWID   21
//#define OLED_CHARLINES 3

// Size of screen when TALL, by default mono font
#define OLED_CHARWID   5
#define OLED_CHARLINES 16

void oled_clear_screen(void) {
    for (int i = 0; i < OLED_CHARLINES; i++) {
        oled_write_P(PSTR("\n"), false); 
    }
}

void oled_render_layer_state(void) {
    
    oled_write_ln_P(PSTR("\n\n"), false);

    // NOTE( IF CONDITION ORDERING ):
    //   The layers have state that allows having 2 layers enabled 
    //   at once. It is a unrolling list of layers to backtrack 
    //   and count things like momentary enabling and tapdances. 
    //   It's broken for LT and other multi-function-hold-to-layer buttons. 
    // 
    //   Point is -- mention deep layers BEFORE the layer before it. 
    //   The basic getter just doesn't know what you really want.
    
    // TODO:  Write an array of strings where we'd use the layer enums
    //   as indexes into the array. We can then just loop through that
    //   to get the strings we want.
    //   The ordering of the indexes will also help replace the condition
    //   ordering.
    //   But all this would require knowing how to deal with all that
    //   in C and in QMK, so I probably won't get around to doing it. 

    if (false) { // a line just so it lines up...

    } else if (layer_state_is(_Num)) {
        oled_write_ln_P(PSTR("\n\n\n  \n 1234"), false);
    } else if (layer_state_is(_Sym)) {
        oled_write_ln_P(PSTR("\n\n\n  ?!*- "), false);

    } else if (layer_state_is(_Special)) {
        oled_write_ln_P(PSTR("\n spec"), false);
    } else if (layer_state_is(_OS_Ultra)) { 
        oled_write_ln_P(PSTR("\n  OS"), false);


    } else if (layer_state_is(_HHcz)) {
        oled_write_ln_P(PSTR("Czech"), false);

    } else if (layer_state_is(_GameX)) {
        oled_write_ln_P(PSTR("  ~~\n\n GAME\n\n  ~~"), false);
    } else if (layer_state_is(_Game)) {
        oled_write_ln_P(PSTR("\n\n GAME"), false);

    } else if (layer_state_is(_Unlock)) {
        oled_write_ln_P(PSTR("\n\n\n\n\n\n..."), false);
    } else if (layer_state_is(_Lock)) {
        oled_write_ln_P(PSTR("\n\n\n\n\n\n\nLOCK!"), false);


    } else if (layer_state_is(_HH2)) {
        oled_write_ln_P(PSTR(" ####\n\n ####\n\n ####\n\n ####"), false);
    } else if (layer_state_is(_HH)) { 
        oled_write_ln_P(PSTR(""), false);


    ////// } else if (layer_state_is(_x2)) {
    //////     oled_write_ln_P(PSTR(" #\n\n #\n\n #\n\n #"), false);
    ////// } else if (layer_state_is(_x)) { 
    //////     oled_write_ln_P(PSTR(" ring"), false);

    } else if (layer_state_is(_MediaX)) {
        oled_write_ln_P(PSTR("  ..\n\n  ()\n"), false);
    } else if (layer_state_is(_Media)) { 
        oled_write_ln_P(PSTR("  ..\n\n \\__/\n"), false);

    } else {
        oled_write_ln_P(PSTR("Layer\nundef\n!!!"), false);
    }
}

void oled_render_WPM(void) {
    // https://github.com/qmk/qmk_firmware/blob/master/docs/feature_wpm.md
    /*
        char s [OLED_WIDTH_CHARACTERS];
        snprintf(s, sizeof(s), "%d", get_current_wpm());
        oled_write_ln(s, false);

        ...these do like 1.5 times more than it should be... I'm not sure
        what else to use though... So probably track it myself!
    */
}

// This dictates the print order. 
#define M_ALT 1
#define M_GUI 0
#define M_CTL 3
#define M_SFT 2

// :: Queued up modifiers with OSM
bool oneshot_mods [4] = {false, false, false, false};
void oneshot_mods_changed_user(uint8_t mods) { // QMK callback
    oneshot_mods[M_SFT] = (mods & MOD_MASK_SHIFT);
    oneshot_mods[M_GUI] = (mods & MOD_MASK_GUI);
    oneshot_mods[M_CTL] = (mods & MOD_MASK_CTRL);
    oneshot_mods[M_ALT] = (mods & MOD_MASK_ALT);
    // if (!mods) { println("Oneshot mods off"); }
}

void oled_modifiers(void) {
    // https://www.reddit.com/r/olkb/comments/covpq3

    // :: Regular MO-like mod presses
    uint8_t mods = get_mods();
    bool _mods [4] = {false, false, false, false};
    _mods[M_SFT] = (mods & MOD_MASK_SHIFT);
    _mods[M_GUI] = (mods & MOD_MASK_GUI);
    _mods[M_CTL] = (mods & MOD_MASK_CTRL);
    _mods[M_ALT] = (mods & MOD_MASK_ALT);
    
    { // We don't print if no mod is active in any way
        bool print = false;
        for (int i = 0; i < 4; i++) {
            if (_mods[i] || oneshot_mods[i]) { print = true; }
        }
        if (!print) { 
            oled_write_P(PSTR("\n\n"), false);
            return;
        }
    }

    oled_write_P(PSTR(" "), false); // centering
    for (int i = 0; i < 4; i++) {
        if (oneshot_mods[i]) {
            switch (i) {
                case M_SFT: oled_write_P(PSTR("s"), false); break;
                case M_ALT: oled_write_P(PSTR("a"), false); break;
                case M_CTL: oled_write_P(PSTR("c"), false); break;
                case M_GUI: oled_write_P(PSTR("g"), false); break;
            }
        } else if (_mods[i]) {
            switch (i) {
                case M_SFT: oled_write_P(PSTR("S"), false); break;
                case M_ALT: oled_write_P(PSTR("A"), false); break;
                case M_CTL: oled_write_P(PSTR("C"), false); break;
                case M_GUI: oled_write_P(PSTR("G"), false); break;
            }
        } else {
            oled_write_P(PSTR("_"), false);
        }
    }
    oled_write_P(PSTR("\n"), false);
}

//


void render_bootmagic_status(bool status) {
    /* Show Ctrl-Gui Swap options */
    static const char PROGMEM logo[][2][3] = {
        {{0x97, 0x98, 0}, {0xb7, 0xb8, 0}},
        {{0x95, 0x96, 0}, {0xb5, 0xb6, 0}},
    };
    if (status) {
        oled_write_ln_P(logo[0][0], false);
        oled_write_ln_P(logo[0][1], false);
    } else {
        oled_write_ln_P(logo[1][0], false);
        oled_write_ln_P(logo[1][1], false);
    }
}

void oled_render_logo(void) {
    static const char PROGMEM crkbd_logo[] = {
        0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94,
        0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4,
        0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4,
        0};
    oled_write_P(crkbd_logo, false);
}

bool  oled_task_user(void) { // QMK callback
    if (is_keyboard_master()) {
        oled_clear_screen();
        oled_modifiers();
        oled_render_layer_state();
    } else {
        oled_render_logo();
    }

    return false;
}

