
#define ___  KC_NO
#define ____ KC_NO

#define LAYOUT_plaid( \
	k00, k01, k02, k03, k04,           k07, k08, k09, k0a, k0b, \
	k10, k11, k12, k13, k14,           k17, k18, k19, k1a, k1b, \
	k20, k21, k22, k23, k24,           k27, k28, k29, k2a, k2b, \
	               k33, k34, k35, k36, k37, k38  \
) \
{ \
	{ ___, ___, k00, k01, k02, k03, k04,      k07, k08, k09, k0a, k0b }, \
	{ ___, ___, k10, k11, k12, k13, k14,      k17, k18, k19, k1a, k1b }, \
	{ ___, ___, k20, k21, k22, k23, k24,      k27, k28, k29, k2a, k2b }, \
	{ ___, ___, ___, ___, k33, k34, k35,      k36, k37, k38, ___, RESET } \
}
#define LAYOUT_plaid_UPSIDEDOWN( \
	k00, k01, k02, k03, k04,           k07, k08, k09, k0a, k0b, \
	k10, k11, k12, k13, k14,           k17, k18, k19, k1a, k1b, \
	k20, k21, k22, k23, k24,           k27, k28, k29, k2a, k2b, \
	               k33, k34, k35, k36, k37, k38  \
) \
{ \
  { RESET, ___, k38, k37, k36, k35, k34, k33, ___, ___, ___, ___ }, \
  {   k2b, k2a, k29, k28, k27, k24, k23, k22, k21, k20, ___, ___ }, \
  {   k1b, k1a, k19, k18, k17, k14, k13, k12, k11, k10, ___, ___ }, \
  {   k0b, k0a, k09, k08, k07, k04, k03, k02, k01, k00, ___, ___ }  \
}

#define LAYOUT_corne( \
  L00, L01, L02, L03, L04,           R00, R01, R02, R03, R04, \
  L10, L11, L12, L13, L14,           R10, R11, R12, R13, R14, \
  L20, L21, L22, L23, L24,           R20, R21, R22, R23, R24, \
                 L30, L31, L32, R30, R31, R32 \
) \
{ \
  { ___, L00, L01, L02, L03, L04 }, \
  { ___, L10, L11, L12, L13, L14 }, \
  { ___, L20, L21, L22, L23, L24 }, \
  { ___, ___, ___, L30, L31, L32 }, \
  { ___, R04, R03, R02, R01, R00 }, \
  { ___, R14, R13, R12, R11, R10 }, \
  { ___, R24, R23, R22, R21, R20 }, \
  { ___, ___, ___, R32, R31, R30 } \
}
#define LAYOUT_minidox( \
  k01, k02, k03, k04, k05,    k45, k44, k43, k42, k41, \
  k11, k12, k13, k14, k15,    k55, k54, k53, k52, k51, \
  k21, k22, k23, k24, k25,    k65, k64, k63, k62, k61, \
            k33, k34, k35,    k75, k74, k73            \
) \
{ \
  { k01, k02, k03, k04, k05 }, \
  { k11, k12, k13, k14, k15 }, \
  { k21, k22, k23, k24, k25 }, \
  { ___, ___, k33, k34, k35 }, \
  { k41, k42, k43, k44, k45 }, \
  { k51, k52, k53, k54, k55 }, \
  { k61, k62, k63, k64, k65 }, \
  { ___, ___, k73, k74, k75 } \
}
#define LAYOUT_lily58( \
  L00, L01, L02, L03, L04,           R00, R01, R02, R03, R04, \
  L10, L11, L12, L13, L14,           R10, R11, R12, R13, R14, \
  L20, L21, L22, L23, L24,           R20, R21, R22, R23, R24, \
                 L30, L31, L32, R30, R31, R32 \
) \
{ \
  { ___, ___, ___, ___, ___, ___ }, \
  { ___, L00, L01, L02, L03, L04 }, \
  { ___, L10, L11, L12, L13, L14 }, \
  { ___, L20, L21, L22, L23, L24 }, \
  { ___, ___, L30, L31, L32, ___ }, \
  { ___, ___, ___, ___, ___, ___ }, \
  { ___, R04, R03, R02, R01, R00 }, \
  { ___, R14, R13, R12, R11, R10 }, \
  { ___, R24, R23, R22, R21, R20 }, \
  { ___, ___, R32, R31, R30, ___ } \
}

#define LAYOUT_SHAPESHIFTER LAYOUT_minidox
//#define LAYOUT_SHAPESHIFTER LAYOUT_corne
//#define LAYOUT_SHAPESHIFTER LAYOUT_lily58
//#define LAYOUT_SHAPESHIFTER LAYOUT_plaid_UPSIDEDOWN



const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  // ========================================================================================================
  [_HH] = LAYOUT_SHAPESHIFTER(
      undo,KC_VOLD,KC_VOLU,KC_MPLY,KC_SLEP,   TO(_Media),TO(_Game),scr_d,scr_u,  RESET,
     
   KC_RCTL,    _S,    _R,    _T,   _ent,       _bsp,   _N,    _E,    _A,    KC_RCTL,
        _Y,    _G,    _L,    _D,   _esc,       _del,   _U,    _I,    _O,    dont,
                 KC_LALT,   _x2,  _spec,       MO(_OS_Ultra),  _spc,  KC_RSFT
  ),
  [_HH2] = LAYOUT_SHAPESHIFTER(
     oooo,oooo,oooo,oooo,oooo,                    oooo,oooo,oooo,oooo,oooo,
     
     oooo,   _F,   _P,   _C,  oooo,         oooo,  _M,   dot,   _K,   oooo,
       _X,   _J,   _V,   _B,  oooo,         oooo,  _H,    _W,   _Z,   _Q,
                  oooo, oooo, oooo,         oooo,  oooo, oooo         
  ),
  [_HHcz] = LAYOUT_SHAPESHIFTER(
      ____, KC_BRID,KC_BRIU, ____,   RESET,        RESET,   ____,  RESET,  RESET,  ____,

    g_kase, M_sss,  M_rrr,  M_ccc, KC_CAPS,        _bsp,  M_e_h,  M_e_c,  M_aaa,  g_kase, 
    M_yyy,  hacek,  carka,  M_zzz, KC_LCTL,        _del,  M_u_k,  M_iii,  M_ooo,  ____,
                     _ent,   _esc,    ____,        ____,   _spc,  FAKESHIFT
  ),
  // ========================================================================================================
  [_OS_Ultra] = LAYOUT_SHAPESHIFTER(
     G(KC_P1),G(KC_P2),G(KC_P3),G(KC_P4),G(KC_P5),              G(KC_P6),G(KC_P7),G(KC_P8),G(KC_P9), G(KC_P0),
    
    KC_RCTL,  KC_HOME,    KC_UP,  KC_LEFT,   _ent,         _bsp,   ms_l,   ms_u,   ms_r,   KC_RCTL, 
    KC_RSFT,  KC_END,   KC_DOWN, KC_RIGHT,   _esc,         _del,   scr_d,  ms_d,   scr_u,  WORKSP(),
                        KC_LALT,  KC_LGUI,  _spec,         KC_LGUI,  _spc,   KC_RSFT                
  ),
  [_Special] = LAYOUT_SHAPESHIFTER(

    KC_F1,    KC_F2,    KC_F3,         KC_F4,    KC_F5,        KC_F6,      KC_F7,    KC_F8,    KC_F9,  KC_F10, 
       _Z,  KC_PDOT,  KC_NLCK,        KC_F11,   KC_F12,        KC_INS,     KC_CAPS,  CL_SWAP,  ____,   HHHH,
  KC_RSFT,     ____,  KC_RSFT,   LALT(KC_F4),  KC_LGUI,        KC_NLCK,    KC_LCTL,  CL_NORM,  ____,   KC_PSCR,
                         HHHH,          HHHH,    _spec,        HHHH,       HHHH,     HHHH                      
  ),
  // ========================================================================================================
  [_Sym] = LAYOUT_SHAPESHIFTER(
     oooo,oooo,oooo,oooo,oooo,                    oooo,oooo,oooo,oooo,oooo,

    semic,  comma,     par_l,    par_r,   _ent,        _bsp,  equ,     colon,  minus,   argh,
     quot,    amp,   curly_l,  curly_r,   _esc,        _del,  htm_l,   slash,  htm_r,   bslash,
                       _symb,      _x2,  _symb,        MO(_Num), _spc,  MO(_Num)                   
  ),
  [_Num] = LAYOUT_SHAPESHIFTER(
                              
      ____,   perc,       at,     chev,      ____,        ____,    _7,    _8,    _9,  ____, 
     hasht,  undsc,    par_l,    par_r,      _ent,        _bsp,    _4,    _5,    _6,  _0,
     bquot,   pipe,   brak_l,   brak_r,      _esc,        _del,    _1,    _2,    _3,  dot,
                       _symb,      _x2,     _symb,        MO(_Num), _spc,  MO(_Num)       
  ),
  // ========================================================================================================

  // [_Game] = LAYOUT_SHAPESHIFTER(                             
  //    KC_TAB,   ___q, ___w,  ___e, ___r,        ___t,    ___y,  ___u,  ___o,   ___p,
  //   KC_LSFT,   ms_l, ms_u,  ms_r, ___f,        KC_SPC,  KC_LEFT,  KC_UP,   KC_RIGHT,  KC_LSFT,
  //   KC_LCTL,  scr_d, ms_d, scr_u, ___v,        KC_MPLY, KC_VOLD,  KC_DOWN, KC_VOLU,   KC_Z,
  //       OSL(_GameX),  KC_SPC,  KC_BTN1,        TO(_HH), KC_X,  KC_LSFT                
  // ),
  
  [_Game] = LAYOUT_SHAPESHIFTER(
                               
    KC_LALT,  ___q,  ___w,  ___e, ___r,        ___t,  ___y,  ___u,  ___o,   ___p,
    KC_LSFT,  ___a,  ___s,  ___d, ___f,        ___g,  ___h,  ___j,  ___k,   ___l,
    KC_LCTL,  ___z,  ___x,  ___c, ___v,        ___b,  ___n,  ___m,   dot,  comma,
        KC_BTN1,  KC_SPC,  OSL(_GameX),        OSL(_GameX), KC_SPC, KC_SPC
  ),
  [_GameX] = LAYOUT_SHAPESHIFTER(
    KC_6,     KC_7,    KC_8,    KC_9,    KC_0,      ____,     KC_WH_U,  KC_MS_U,  KC_WH_D,  KC_ESC,  
    KC_1,     KC_2,    KC_3,    KC_4,    KC_5,      ____,     KC_MS_L,  KC_MS_D,  KC_MS_R,  KC_ACL2, 
    ____,  KC_LALT,  KC_TAB,  KC_ESC,  KC_ENT,      ____,     HHHH,     KC_BTN3,  ____,     HHHH,    
                       HHHH,    HHHH,    oooo,      KC_BTN2,  KC_BTN1,  HHHH
  ),
  // ======================================================================================================== 
  [_Media] = LAYOUT_SHAPESHIFTER( 
       HHHH,  scr_d,    ms_u,     scr_u,        KC_ESC,           KC_ESC,       scr_d,    ms_u,     scr_u,    HHHH,
     KC_SPC,  ms_l,     ms_d,     ms_r,         KC_ENT,           KC_ENT,       ms_l,     ms_d,     ms_r,     KC_SPC,
    KC_BTN2,  KC_LSFT,  KC_LALT,  KC_TAB,       KC_F11,           KC_F11,       KC_TAB,   KC_LALT,  KC_LSFT,  KC_BTN2,
                         KC_SPC,  KC_BTN1,  MO(_MediaX),          MO(_MediaX),  KC_BTN1,  KC_SPC

  ),
  [_MediaX] = LAYOUT_SHAPESHIFTER(
       HHHH,  KC_HOME,    KC_UP,        KC_END,  KC_VOLU,        KC_VOLU,  KC_HOME,      KC_UP,    KC_END,   HHHH,
     KC_SPC,  KC_LEFT,  KC_DOWN,      KC_RIGHT,  KC_VOLD,        KC_VOLD,  KC_LEFT,      KC_DOWN,  KC_RIGHT, KC_SPC,
    KC_BTN3,  KC_LGUI,  KC_BRID,       KC_BRIU,  KC_SLEP,        KC_SLEP,  KC_BRID,      KC_BRIU,  KC_LGUI,  KC_BTN3,
                           HHHH,  MO(_Special),     oooo,        oooo,     MO(_Special), HHHH

  ),
#ifdef DEPRECATED_LAYERS
  // ========================================================================================================
  [_Lock] = LAYOUT_SHAPESHIFTER( // using some combos here -- they're pretty great for this! 
    ____, ____,  ____, ____, ____,        ____, ____, ____,  ____, ____, 
    ____, ____, Unl_1, ____, ____,        ____, ____, Unl_1, ____, ____, 
    ____, ____, Unl_2, _UP_, ____,        ____, _UP_, Unl_2, ____, ____,
                ____,  ____, ____,        ____, ____, ____   
  ),
  [_Unlock] = LAYOUT_SHAPESHIFTER( // and there's a fallback without combos 
    _DW_, _DW_, _DW_, _DW_, _DW_,        _DW_, _DW_, _DW_, _DW_, _DW_, 
    _DW_, _DW_, _DW_, _DW_, _DW_,        _DW_, _DW_, _DW_, _DW_, _DW_, 
    _DW_, HHHH, _DW_, ____, KC_SLEP,  KC_SLEP, ____, _DW_, HHHH, _DW_, 
                _DW_, _DW_, _DW_,        _DW_, _DW_, _DW_
  ),
#endif //DEPRECATED_LAYERS
};
