


# SteamDeck flashing -- "Waiting for ... to become writable" issue

> Okay, *normal* people won't do this, but I did. 
> Maybe it'll be helpful one day to someone. 

- I'm using `distrobox` to run QMK. 
- Steam Deck always has either permission or immutable-filesystem issues. 
- This was specific to the `catarina` bootloader -- ProMicro board default. 
  Other boards might not have the issue.

Here's what got me towards the solution... 
[A reddit post](https://www.reddit.com/r/olkb/comments/pwhthj/stuck_on_waiting_for_devttyamc0_to_become_writable/)

I was told that I need permissions to `/dev/ttyACM1` if I want to make 
it writable... So, I need to spam a `chmod` as soon as it appears, 
after I restart the board -- the file isn't there until it is restarted,
hence the spamming.

```shell
# From the usual Deck shell, not `distrobox enter` -- it won't let you
sleep .5 ; sudo chmod 777 /dev/ttyACM* ; ls -lha /dev/ttyACM* ;
sleep .5 ; sudo chmod 777 /dev/ttyACM* ; ls -lha /dev/ttyACM* ;
sleep .5 ; sudo chmod 777 /dev/ttyACM* ; ls -lha /dev/ttyACM* ;
# And just spam it more and more into the terminal
```

> Overkill Solution -- [Don't use `catarina` bootloader](https://www.reddit.com/r/olkb/comments/8sxgzb/replace_pro_micro_bootloader_with_qmk_dfu/)
>
> ... But that's absolutely stupid. And requires some equipment.