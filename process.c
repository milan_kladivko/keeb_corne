
/*
  
  For the relevant keycodes see `custom_codes.c`. 
  They must be, unfortunately, quite far from the code that uses them. 

 */



//
//  Utilities
// 

#define tap(code) tap_code16(code)
// return value for `bool process_record_user(`
bool PROCESS_PASS  = true;  // Process all other keycodes normally
bool PROCESS_BLOCK = false; // Stop the press processing

void press(bool pressed, uint16_t kc) {
    if (pressed)  register_code  (kc);
    else          unregister_code(kc);
}
void tapp(bool pressed, uint16_t kc) {
    if (pressed) {
        register_code16(kc);
        unregister_code16(kc);
    }
    // else do nothing
}


//
//  Minor functions
//

// Aliases for alphas that shouldn't be treated as macro-keys.
bool raw_alphas(uint16_t keycode, keyrecord_t *record) {
    bool down = record->event.pressed;

    // TODO:  Can we also do the same thing for mouse movements?
    //   They're affected by macros with its delay... I'd like to solve it. 

    switch (keycode) {
    case ___a: press(down, _A); return PROCESS_BLOCK; 
    case ___b: press(down, _B); return PROCESS_BLOCK; 
    case ___c: press(down, _C); return PROCESS_BLOCK; 
    case ___d: press(down, _D); return PROCESS_BLOCK; 
    case ___e: press(down, _E); return PROCESS_BLOCK; 
    case ___f: press(down, _F); return PROCESS_BLOCK; 
    case ___g: press(down, _G); return PROCESS_BLOCK; 
    case ___h: press(down, _H); return PROCESS_BLOCK; 
    case ___i: press(down, _I); return PROCESS_BLOCK; 
    case ___j: press(down, _J); return PROCESS_BLOCK; 
    case ___k: press(down, _K); return PROCESS_BLOCK; 
    case ___l: press(down, _L); return PROCESS_BLOCK; 
    case ___m: press(down, _M); return PROCESS_BLOCK; 
    case ___n: press(down, _N); return PROCESS_BLOCK; 
    case ___o: press(down, _O); return PROCESS_BLOCK; 
    case ___p: press(down, _P); return PROCESS_BLOCK; 
    case ___q: press(down, _Q); return PROCESS_BLOCK; 
    case ___r: press(down, _R); return PROCESS_BLOCK; 
    case ___s: press(down, _S); return PROCESS_BLOCK; 
    case ___t: press(down, _T); return PROCESS_BLOCK; 
    case ___u: press(down, _U); return PROCESS_BLOCK; 
    case ___v: press(down, _V); return PROCESS_BLOCK; 
    case ___w: press(down, _W); return PROCESS_BLOCK; 
    case ___x: press(down, _X); return PROCESS_BLOCK; 
    case ___y: press(down, _Y); return PROCESS_BLOCK; 
    case ___z: press(down, _Z); return PROCESS_BLOCK; 
    }
    return PROCESS_PASS;
}

// Symbols as macros because otherwise, QMK bleeds modifiers through
// sometimes. I'm sure there's a good reason for that or whatever >:(
// 
// Symbols should *always* do the same thing regardless
// of other context of the keyboard. Or trigger a combo if multiple
// keycodes are pressed, in which case the combo sends a different
// keycode. 
//
bool tap_symb(uint16_t keycode) {    
    bool shift = false;
    bool alt   = false;
    switch (keycode) {
    case star: case bquot: case bslash: case pipe:  // all the alt-numbers
    case dollar: case amp: case at: case argh: case squig:
    case perc: case hasht: case chev:
    case par_l: case curly_l: case brak_l: case htm_l:  // all the parens
    case par_r: case curly_r: case brak_r: case htm_r:
        alt = true;   break;
    case _1: case _2: case _3: case _4: case _5:  // all the numbers 
    case _6: case _7: case _8: case _9: case _0:
    case dont: case colon:  // and some shifted symbols in lower-right
        shift = true; break;
    }
    

    if (shift) register_code(KC_RSFT);
    if (alt)   register_code(KC_RALT);
    //if (shift || alt) SEND_STRING(SS_DELAY(200));
    
    bool wrote = true;
    switch (keycode) {
    case comma   : tap_code16( KC_COMM ); break;
    case dot     : tap_code16( KC_DOT  ); break;
    case plus    : tap_code16( KC_1    ); break;
    case minus   : tap_code16( KC_SLSH ); break;
    case star    : tap_code16( KC_8    ); break;
    case equ     : tap_code16( KC_MINS ); break;
    case undsc   : tap_code16( KC_QUES ); break;
    case semic   : tap_code16( KC_GRV  ); break;
    case dont    : tap_code16( KC_NUHS ); break;
    case quot    : tap_code16( KC_COLN ); break;
    case bquot   : tap_code16( KC_GRV  ); break;
    case slash   : tap_code16( KC_LCBR ); break;
    case bslash  : tap_code16( KC_BSLS ); break;
    case par_l   : tap_code16( KC_9    ); break;
    case par_r   : tap_code16( KC_0    ); break;
    case curly_l : tap_code16( KC_LCBR ); break; // shouldn't this be something shifted?
    case curly_r : tap_code16( KC_RCBR ); break; // shouldn't this be something shifted? 
    case brak_l  : tap_code16( KC_LBRC ); break;
    case brak_r  : tap_code16( KC_RBRC ); break;
    case htm_l   : tap_code16( KC_COMM ); break;
    case htm_r   : tap_code16( KC_DOT  ); break;
    case pipe    : tap_code16( KC_PIPE ); break;
    case colon   : tap_code16( KC_DOT  ); break;
    case ques    : tap_code16( KC_LT   ); break;
    case dollar  : tap_code16( KC_4    ); break;
    case amp     : tap_code16( KC_7    ); break;
    case at      : tap_code16( KC_2    ); break;
    case argh    : tap_code16( KC_1    ); break;
    case squig   : tap_code16( KC_TILD ); break;
    case perc    : tap_code16( KC_5    ); break;
    case hasht   : tap_code16( KC_3    ); break;
    case chev    : tap_code16( KC_6    ); break;
    case _1: tap_code16( KC_1 ); break;
    case _2: tap_code16( KC_2 ); break;
    case _3: tap_code16( KC_3 ); break;
    case _4: tap_code16( KC_4 ); break;
    case _5: tap_code16( KC_5 ); break;
    case _6: tap_code16( KC_6 ); break;
    case _7: tap_code16( KC_7 ); break;
    case _8: tap_code16( KC_8 ); break;
    case _9: tap_code16( KC_9 ); break;
    case _0: tap_code16( KC_0 ); break;
    default: wrote = false;
    }

    //if (shift || alt) SEND_STRING(SS_DELAY(200));
    if (shift) unregister_code(KC_RSFT);
    if (alt)   unregister_code(KC_RALT);

    if (wrote) {
        return PROCESS_BLOCK;
    } else {
        return PROCESS_PASS;
    }
}
uint16_t get_base_key(uint16_t keycode) {
    switch (keycode) {
    case QK_MOD_TAP ... QK_MOD_TAP_MAX:     // MT()
    case QK_LAYER_TAP ... QK_LAYER_TAP_MAX: // LT()
    case QK_TAP_DANCE ... QK_TAP_DANCE_MAX: // not using tap dance tho
        return keycode & 0xFF; // truncate extra layer bits
    default:
        return keycode;        // if it's not a special key, do nothing
    }
}
bool symb(uint16_t keycode, keyrecord_t *record) {
    // when in the keymap, trigger only on keydown, let the upstroke be
    bool down = record->event.pressed;
    if ( down ) 
        return tap_symb(keycode); // tap our symbols on downstrokes
    
    return PROCESS_PASS;
}

bool basic_code_macros(uint16_t keycode, keyrecord_t *record) {
    if (record->event.pressed == false) return PROCESS_PASS;

    switch (keycode) {
    case JS_FN: 
        SEND_STRING("function");
        return PROCESS_BLOCK;
    case COMMENT: // // *
        tap_symb(slash);
        tap_symb(slash);
        tap(KC_SPC);
        return PROCESS_BLOCK;

    case CURLIES: // {*}
        tap_symb(curly_l);
        tap_symb(curly_r);
        tap(KC_LEFT);
        return PROCESS_BLOCK;
    case PARENS:  // (*)
        tap_symb(par_l);
        tap_symb(par_r);
        tap(KC_LEFT);
        return PROCESS_BLOCK;
    case BRAKS:   // [*]
        tap_symb(brak_l);
        tap_symb(brak_r);
        tap(KC_LEFT);
        return PROCESS_BLOCK;

    case BRAK_OPEN_NEWLINE: // you know what I mean, right? 
        tap(KC_ENT);
        tap(KC_ENT);
        tap(KC_UP);
        tap(KC_TAB);
        return PROCESS_BLOCK;

    case IMPORT_TS: // import {} from "*"
        SEND_STRING("import ");
        tap_symb(curly_l); tap_symb(curly_r);
        SEND_STRING(" from ");
        tap_symb(quot); tap_symb(quot);
        tap(KC_LEFT);

    case CLASSDIV_HTML: // <div class=
        tap_symb(htm_l);
        SEND_STRING("div class");
        tap_symb(equ);

    case SELECTLINE: 
        // TODO:  We might need to pop+push the shift states. 
        tap(KC_HOME);
        register_code(KC_RSFT);
        tap(KC_END);
        unregister_code(KC_RSFT);
        return PROCESS_BLOCK;
    }

    return PROCESS_PASS;
}


//
//  CZ Keys
//

bool fakeshift = false; 
// TODO:  The fakeshift is ass!
//   Instead, pop and push the shift's state so we still use shift but 
//   the accent is unaffected by the shift before we do things. 
//   It's gonna save some code complexity. 
bool fakeshift_and_homemods(uint16_t keycode, keyrecord_t *record) {

    if (FAKESHIFT == keycode) {
        fakeshift = (record->event.pressed) ? true : false;
    }

    uint16_t kc = keycode;
    bool home_blank = ( kc == ____     && IS_LAYER_ON(_HH) );
    bool to_cz      = ( kc == MO(_HHcz) );

    if (to_cz) {
        if (record->event.pressed) {
            // Carry the fakeshift from a real shift, the moment we do a cz.
            fakeshift = (get_mods() & MOD_MASK_SHIFT);
        } else {
            // When cz is lifted, we'll reset the fakeshift. 
            // It is used only by the cz macros anyway. 
            fakeshift = false;
        }
    }

    if (record->event.pressed && (home_blank || to_cz)) {
        // Let's clear ALL of the mods if we do a dead key on home...
        clear_mods();
        clear_oneshot_mods();
        clear_oneshot_locked_mods();
    }

    return PROCESS_PASS;
}
bool cz_keys(uint16_t keycode, keyrecord_t *record) {
    if (record->event.pressed == false) return PROCESS_PASS;
    bool s = fakeshift;

    // First, the accent type...
    switch (keycode) {
    case M_e_h: case M_rrr: case M_sss: case M_ccc: case M_zzz:
    case M_ddd: case M_ttt: case M_nnn: 
        tap(hacek);
        break;
    case M_e_c: case M_u_c: case M_ooo: case M_aaa: case M_iii: case M_yyy:
        tap(carka);
        break;
    case M_u_k:
        tap(krouzek);
        break;
    }

    // Second, the key with the correct case...
    if (s) register_code(KC_RSFT);
    switch (keycode) {
      case M_e_h: case M_e_c: tap(KC_E); break;
      case M_u_c: case M_u_k: tap(KC_U); break;

      case M_aaa: tap(KC_A); break;
      case M_ooo: tap(KC_O); break;
      case M_iii: tap(KC_I); break;
      case M_yyy: tap(KC_Y); break;
      
      case M_rrr: tap(KC_R); break;
      case M_sss: tap(KC_S); break;
      case M_ccc: tap(KC_C); break;
      case M_zzz: tap(KC_Z); break;
      case M_ddd: tap(KC_D); break;
      case M_ttt: tap(KC_T); break;
      case M_nnn: tap(KC_N); break;
    }
    if (s) unregister_code(KC_RSFT);

    return PROCESS_PASS;
}



//
//  The Major Function
// 

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

// -------------------------------------  debug prints of events  ------------
#ifdef CONSOLE_ENABLE 
    uprintf(
        "KL: kc: %u, col: %u, row: %u, pressed: %u\n", 
        keycode, record->event.key.col, record->event.key.row, 
        record->event.pressed);
#endif // CONSOLE_ENABLE
// --------------------------------------------------------  end  ------------
    
    if (raw_alphas             (keycode, record) == PROCESS_BLOCK) return PROCESS_BLOCK;
    if (fakeshift_and_homemods (keycode, record) == PROCESS_BLOCK) return PROCESS_BLOCK;
    if (basic_code_macros      (keycode, record) == PROCESS_BLOCK) return PROCESS_BLOCK;
    if (cz_keys                (keycode, record) == PROCESS_BLOCK) return PROCESS_BLOCK;
    if (symb                   (keycode, record) == PROCESS_BLOCK) return PROCESS_BLOCK;

    return PROCESS_PASS;
}

