
#define LOCK_ON_START false//MASTER_LEFT

enum layer_order {
    
    // NOTE:  `map.c:keymaps` array only fills the indexes,
    //   the order is defined here, always.
    //   Keeping the order in the array definition is entirely optional. 
    
    // NOTE:  MO/LT can only reach higher layers, it can't go lower.
    //   Keep that in mind when ordering. 

    // NOTE:  This affects how `oooo` (KC_TRNS) fallthroughs work,
    //   so be careful when you lean on it! 

#if LOCK_ON_START
    // On the left, we have the oled to let us know we are locked,
    // so I think it is fine to boot up as locked. 
    _Lock = 0, //--
#endif
    

    // The home alphas -- most of typing is spent here.
    _HH, _HH2, _HHcz,

    // The copypasting, text selection and arrows, mouse and so on
    _OS_Ultra, // arrows, mouse, copypaste, undo and GUI
    _Special,  // Fkeys, media keys and so on. Also has the reset button. 

    // Old symbols remain for now -- I am used to them way too much
    _Sym, _Num,

    // Game layer with the usual QWERTY without fancy functions.
    // Supports left-hand-only operation. 
    _Game,  _GameX,
    
    // One handed media layer with arrows, mouse
    // and volume, alt tabbing, F11, sleep, brightness etc.
    // in easy-to-access places.
    // Should be "mirrored" to both hands the same way to allow
    // using only one connected half of the keyboard to do everything. 
    _Media, _MediaX,

    
#if ! LOCK_ON_START
    // When on the right, I don't like being locked by default.
    // It's weird to deal with when testing if something works
    // or when I need to quickly do something. 
    _Lock, //--
#endif
    
    _Unlock, //--
};
