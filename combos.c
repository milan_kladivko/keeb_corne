
/*
  Better way to do these combos -- powered by macros. 
  https://docs.qmk.fm/#/feature_combo?id=dictionary-management

  Combo definitions are in `./combos.def`. 
  The file's automagically imported and used by that #include right below. 
*/
#include "g/keymap_combo.h"

// See config.h :: #define COMBO_TERM_PER_COMBO true
uint16_t get_combo_term(uint16_t index, combo_t *combo) { // qmk
    
    return COMBO_TERM; // use default -- should be pretty short

    // NOTE: The combos don't check the current layer and so on.
    //  If the keycode is somewhere else, the lag will affect all
    //  occurences of the keycode.
    //  Of course, you won't see the keycodes here and you'll have
    //  to check `combos.def` instead.
    //  It should be fine for most symbols though.
    //  Just keep mouse and arrow keys with as little lag as possible!!

    // long as exceptions..
    switch (index) {
        // all those column combos, impossible to misfire
    case c_LAlt: // actually esc
    case c_RAlt: // actually enter
    case c_LTab:  case c_RTab: 
        return COMBO_TERM * 5; // this should be able to be really huge, actually
    case c_jsFn: 
        return COMBO_TERM * 3;
    
        // all the ones that -should- be a bit more
    case c_Open:
    case c_CutPaste:
    case c_lock:
    case c_toGame: 
        return COMBO_TERM * 3;
    }
    
    // default == short
    return COMBO_TERM; // use default -- should be pretty short
}


/* ========================================================================== 
      ( old way to do it without the macros )
enum combos { 
  BRAK_L_BRAK_R_PIPE,
  A_B_TO_OS,
  C_Codeblock, 

  __combo_len__
};
uint16_t COMBO_LEN = __combo_len__; 

const uint16_t PROGMEM c_Codeblock[] = { curly_l, curly_r, COMBO_END };
const uint16_t PROGMEM brak_l_brak_r_pipe[] = { brak_l, brak_r, COMBO_END};
const uint16_t PROGMEM a_b_to_os[] = { KC_A, KC_B, COMBO_END};

combo_t key_combos[] = {
  [BRAK_L_BRAK_R_PIPE] = COMBO(brak_l_brak_r_pipe,  pipe),
  [A_B_TO_OS]          = COMBO(a_b_to_os,           TO(_OS)),
  [C_Codeblock] = COMBO(c_Codeblock,   CODEBLK),
};
*/


