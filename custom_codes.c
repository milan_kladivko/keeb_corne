
enum custom_keycodes { // ...see `process_record_user()`

    __set_starting_int__ = SAFE_RANGE,  // Start at this int value (in uint_16 range)

    // Used for CZ stuff where we need shift but not really.
    FAKESHIFT,
    // And here are all the CZ characters, later handled with macros...
    //__cz_start__, // In case we need to define the range
    M_e_h, M_rrr, M_sss, M_ccc, M_zzz, M_ddd, M_ttt, M_nnn, // hacek
    M_e_c, M_u_c, M_ooo, M_aaa, M_iii, M_yyy,               // carka
    M_u_k,                                                  // krouzek
    //__cz_end__,

    // Unlocking with a combo
    Unl_1, Unl_2,


    // Symbol macros (see process.c for the funcs called)
    CURLIES, PARENS, BRAKS,
    COMMENT,
    SELECTLINE,
    IMPORT_TS, CLASSDIV_HTML,
    BRAK_OPEN_NEWLINE,
    JS_FN,

    // Custom keycodes for alphas that shouldn't be treated as macro-keys.
    // -> `bool raw_alphas()`
    // NOTE:  Leave it in a column like this so we can copypaste the list
    //   and modify/use it for other things.
    // NOTE:  Mostly for use where latency matters, i.e. the Game layers.
    // NOTE:  Is it possible to just ignore functionality such as macros
    //   and so on without doing this roundabout thing?? It's a hassle! 
    ___a,
    ___b,
    ___c,
    ___d,
    ___e,
    ___f,
    ___g,
    ___h,
    ___i,
    ___j,
    ___k,
    ___l,
    ___m,
    ___n,
    ___o,
    ___p,
    ___q,
    ___r,
    ___s,
    ___t,
    ___u,
    ___v,
    ___w,
    ___x,
    ___y,
    ___z,
    ___SPC,
    

    // symbols
    comma,
    dot,
    plus,
    minus,
    star,
    equ,
    undsc,
    semic,
    dont,
    quot,
    bquot,
    slash,
    bslash,
    par_l, par_r,
    curly_l, curly_r,
    brak_l, brak_r,
    htm_l, htm_r,
    pipe,
    colon,
    ques,
    dollar,
    amp,
    at,
    argh,
    squig,
    perc,
    hasht,
    chev,
    _1, _2, _3, _4, _5, _6, _7, _8, _9, _0,
};


//
//  And aliases, with all their goddamn drawbacks... 
//


// aliases for frequently used keys 
#define ____ KC_NO   // do nothing (still affects one-shot stuff though!!)
#define HHHH TO(_HH) // "to home layer"
#define oooo KC_TRNS // fallthrough and use a "lower layer"; see `enum layer_order`

// [COMPAT::2023-02-06]  They removed RESET and instead we have 
//   a "reboot", which can't be used for flashing and "boot" which can be. 
//   I used this only for flashing and I reboot by simply unplugging, 
//   so the choice is clear here...
#define RESET QK_BOOT
// [COMPAT::2023-02-06]  Also, I guess _NLCK sounded stupid so it's _NUM now.
#define KC_NLCK KC_NUM_LOCK


// Locking mechanism
#define _UP_ MO(_Unlock)
#define _DW_ TO(_Lock)

// for some reason, QMK didn't have a CTRL+GUI predefined... what gives?
#define WORKSP_W(kc) (QK_RCTL | QK_LGUI | (kc))
#define WORKSP()     (QK_RCTL | QK_LGUI)
// https://github.com/qmk/qmk_firmware/blob/master/docs/mod_tap.md
#define WSP_T(kc)  MT(MOD_RCTL | MOD_LGUI, (kc))

enum alpha_aliases {
    // These are just aliases and nothing else, they can be used interchangably
    // with KC_* in any case.

    // NOTE:  Here, I'm using home row mods defined within the alpha aliases
    //   so that it doesn't mess with alignment of the map and I don't have
    //   to rewrite these there.
    
    // TODO:  Add in some modifiers and redundancy. There are situations
    //   where there would be a nice alternative position.
    
    //                             SYM NUM OS   |   OS NUM SYM
    //                             s   r   t    |    n   e   a
    //                             g   l   d    |    u   i   o
    //                             OS  CZ       |       CZ SYM
    _A = LT(_Sym, KC_A),
    _B = KC_B,
    _C = KC_C,
    _D = KC_D,
    _E = LT(_Num, KC_E),
    _F = KC_F,
    _G = LT(_OS_Ultra, KC_G),
    _H = KC_H,
    _I = LT(_HHcz, KC_I),
    _J = KC_J,
    _K = KC_K,
    _L = LT(_HHcz, KC_L),
    _M = KC_M,
    _N = LT(_OS_Ultra, KC_N),
    _O = LT(_Sym, KC_O),
    _P = KC_P,
    _Q = KC_Q,
    _R = LT(_Num, KC_R),
    _S = LT(_Sym, KC_S),
    _T = LT(_OS_Ultra, KC_T),
    _U = KC_U,
    _V = KC_V,
    _W = KC_W,
    _X = KC_X,
    _Y = KC_Y,
    _Z = KC_Z,

    /*  If we ever need to go back to raw aliases, here they are...
    _A = KC_A,
    _B = KC_B,
    _C = KC_C,
    _D = KC_D,
    _E = KC_E,
    _F = KC_F,
    _G = KC_G,
    _H = KC_H,
    _I = KC_I,
    _J = KC_J,
    _K = KC_K,
    _L = KC_L,
    _M = KC_M,
    _N = KC_N,
    _O = KC_O,
    _P = KC_P,
    _Q = KC_Q,
    _R = KC_R,
    _S = KC_S,
    _T = KC_T,
    _U = KC_U,
    _V = KC_V,
    _W = KC_W,
    _X = KC_X,
    _Y = KC_Y,
    _Z = KC_Z,
    */
};

enum mouse_aliases {
    scr_u = KC_WH_U,
    scr_d = KC_WH_D,
    ms_u  = KC_MS_U,
    ms_d  = KC_MS_D,
    ms_l  = KC_MS_L,
    ms_r  = KC_MS_R,
    ms_1  = KC_BTN1,
    ms_2  = KC_BTN2,
    ms_3  = KC_BTN3,
};

enum common_shortcuts {
    undo = RCTL(KC_Z), save = RCTL(KC_S),
    cut = RCTL(KC_X), copy = RCTL(KC_C), paste = RCTL(KC_V),
    find = RCTL(KC_F), 
    aucom = RCTL(KC_T),
};
enum permanent_keys {
    // To not use `oooo` fallthroughs everywhere, I'll use some aliases
    // for the keys that are used a lot and should rarely ever move / be replaced.
    _spc = KC_SPC,
    _ent = KC_ENT,
    _tab = WSP_T(KC_TAB), // ctrl+gui to switch workspaces
    _bsp = KC_BSPC,
    _esc = KC_ESC,
    _del = KC_DEL,
    // and let's do a lot of the layer switches too
    _x2   = OSL(_HH2),
    _spec = MO(_Special),
    _symb = OSL(_Sym),
    _game = TO(_Game),
    _medi = TO(_Media),
};

enum czech_mods {
    hacek   = LSFT(KC_EQL),
    carka   = KC_EQL,
    krouzek = LSFT(KC_GRV),
    g_kase  = KC_NUHS,
};
