
MOUSEKEY_ENABLE = yes

EXTRAKEY_ENABLE = yes

COMBO_ENABLE = yes
# Enabling combo-macros to make creating combos (chords) easier. 
VPATH += keyboards/gboards

NO_ACTION_MACRO = yes
NO_ACTION_FUNCTION = yes
DISABLE_LEADER = yes

COMMAND_ENABLE = no
UNICODE_ENABLE = no

#

OLED_ENABLE = no
AUDIO_ENABLE = no

BOOTMAGIC_ENABLE = no

# Optimizing the compile size
# https://github.com/qmk/qmk_firmware/issues/3224
# https://thomasbaart.nl/2018/12/01/reducing-firmware-size-in-qmk/
EXTRAFLAGS  += -flto
